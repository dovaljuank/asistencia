import evdev
import threading
import requests
import json
import os
import time
import base64
from queue import Queue
result_queue = Queue()

# Diccionario para mapear códigos de barras a caracteres ASCII
barcode_ascii_mapping = {
    "KEY_1": "1",
    "KEY_2": "2",
    "KEY_3": "3",
    "KEY_4": "4",
    "KEY_5": "5",
    "KEY_6": "6",
    "KEY_7": "7",
    "KEY_8": "8",
    "KEY_9": "9",
    "KEY_0": "0",
    "KEY_A": "A",
    "KEY_E": "E",
    "KEY_I": "I",
    "KEY_O": "O",
    "KEY_F": "F",
    "KEY_M": "M",
    "KEY_ENTER": "\n",
    "KEY_KPENTER": "\n",
    "KEY_SPACE": " ",
    "KEY_TAB": "\t",
    "KEY_BACKSPACE": "\b",
    "KEY_ESC": "ESC",
    "KEY_F1": "F1",
    "KEY_F2": "F2",
    "KEY_F3": "F3",
    "KEY_SLASH": "?",
}

integer_value = []
photo_path = []
def find_barcode_reader_path():
    # Obtener la lista de dispositivos evdev
    devices = [evdev.InputDevice(fn) for fn in evdev.list_devices()]

    for device in devices:
        # Verificar si el nombre del dispositivo contiene "2D" y "HID"
        if "2D" in device.name and "HID" in device.name:
            return device.fn  # Devolver la ruta del dispositivo

    return None  # Devolver None si no se encuentra el lector de códigos de barras

def read_barcode_thread(barcode_reader_path):
    device = evdev.InputDevice(barcode_reader_path)
    print("Leyendo códigos de barras. Presiona Ctrl+C para salir.")
    
    # Variable para acumular caracteres leídos
    barcode_data = ""

    for event in device.read_loop():
        if event.type == evdev.ecodes.EV_KEY and event.value == 1:
            # El evento es una tecla presionada (valor 1)
            key_event = evdev.categorize(event)
            barcode_key = key_event.keycode

            # Obtener el carácter ASCII correspondiente al código de barras
            if barcode_key in barcode_ascii_mapping:
                barcode_char = barcode_ascii_mapping[barcode_key]
                barcode_data += barcode_char  # Acumular el carácter leído

                if barcode_char in ['M', 'F', 'A', 'O', 'E', 'I']:
                    # Imprimir los 10 caracteres antes de la 'M' o 'F' como un solo número entero
                    last_10_chars = barcode_data[-11:-1].lstrip('0')  # Eliminar ceros a la izquierda
                    integer_value = int(last_10_chars) if last_10_chars else 0  # Convertir a entero
                    print(f"\nCódigo de barras leído (como un solo número entero): {integer_value}", end='', flush=True)
                    result_queue.put(integer_value)
                    # return integer_value
                    break  # Detener la lectura después de imprimir los caracteres requeridos

# Dirección IP del ESP32-CAM y puerto del servidor web
esp32_cam_ip = "192.168.1.67"  # Cambia esto con la IP de tu ESP32-CAM
esp32_cam_port = 80

# Directorio de escritorio
desktop_path = os.path.expanduser("/home/inocuo/Desktop")

def activate_led():
    # Activar el LED en el ESP32-CAM
    led_url = f"http://{esp32_cam_ip}:{esp32_cam_port}/control?var=led=on"
    try:
        requests.get(led_url, timeout=10)
        print("LED activado en el ESP32-CAM.")
    except requests.exceptions.RequestException as e:
        print(f"Error al activar el LED: {e}")

def capture_photo(integer_value):
    # Activar el LED antes de la captura
    activate_led()

    # Capturar foto
    capture_url = f"http://{esp32_cam_ip}:{esp32_cam_port}/capture"
    try:
        response = requests.get(capture_url, timeout=10)
        if response.status_code == 200:
            # Guardar la foto en el escritorio
            photo_path = os.path.join(desktop_path, f"photo_{int(time.time())}.jpg")
            with open(photo_path, "wb") as photo_file:
                photo_file.write(response.content)
            print(f"Foto capturada y guardada en: {photo_path}")

            # Enviar datos a la API
            print(f"#####################3: {integer_value}")
            send_data_to_api(integer_value, photo_path)

            # Imprimir mensaje después de capturar la foto
            print("Datos enviados a la API.")

        else:
            print(f"Error al capturar la foto. Código de estado: {response.status_code}")
    except requests.exceptions.RequestException as e:
        print(f"Error de conexión: {e}")


def authenticate(email, password):
    auth_url = "https://asistencia.dilushop.com.co/api/v1/login"  # Reemplaza con la URL real de inicio de sesión

    # Datos de inicio de sesión
    credentials = {"email": email, "password": password}

    try:
        # Realizar la solicitud POST para obtener el token
        response = requests.post(auth_url, json=credentials)

        # Verificar si la solicitud fue exitosa (código 200)
        if response.status_code == 200:
            # Obtener el token desde la respuesta JSON
            data = response.json().get("data", {})
            token = data.get("token")
            print(f"TOKEN: {token}")

            if token:
                print("Inicio de sesión exitoso.")
                return token
            else:
                print("Error: No se recibió un token en la respuesta del servidor.")
                return None
        else:
            print(f"Error en la solicitud de inicio de sesión. Código de estado: {response.status_code}")
            return None

    except requests.exceptions.RequestException as e:
        print(f"Error al conectar con el servidor de autenticación: {e}")
        return None

def send_data_to_api(integer_value, photo_path):
    # Construir el cuerpo del JSON con el código de barras como entero y la foto codificada en base64
    # api_data = {
    #     "id_card": integer_value,
    #     "photo":''
    # }

    # print(f"photo_path {photo_path}")

    # files = {'photo': ('photo.jpg', open(photo_path, 'rb'), 'image/jpg')}
    # api_data = {'photo': integer_value}
    # print(f'api_data: {api_data}')
    # print(f'files: {files}')

    # with open(photo_path, "rb") as photo_file:
    #     encoded_photo = base64.b64encode(photo_file.read()).decode('utf-8')

    # # Construir el cuerpo del formulario con el código de barras como entero y la foto codificada en base64
    # form_data = {
    #     'id_card': str(integer_value),  # Asegúrate de convertir integer_value a cadena si no lo es
    #     'photo': encoded_photo,
    # }

    # print(f'form_data: {form_data}')

    form_data = {
        'id_card': str(integer_value),  # Asegúrate de convertir id_card a cadena si no lo es
    }

    files = {
        'photo': ('juanxd.jpg', open(photo_path, 'rb'), 'image/jpeg')
    }

    token = authenticate('test@example.com', '123456789')

    headers = {"Authorization": f"Bearer {token}"}

    # URL de la API a la que se enviarán los datos
    api_url = "https://asistencia.dilushop.com.co/api/v1/attendances"

    try:
        # Enviar la solicitud POST a la API
        response = requests.post(api_url, data=form_data,  files=files, headers=headers)
        if response.status_code == 200:
            print("Datos enviados correctamente a la API.")
        else:
            print(f"Error al enviar datos a la API. Código de estado: {response.status_code}")

    except requests.exceptions.RequestException as e:
        print(f"Error al conectar con la API: {e}")

def encode_photo_base64(photo_path):
    # Codificar la foto en base64
    with open(photo_path, "rb") as photo_file:
        encoded_photo = base64.b64encode(photo_file.read()).decode('utf-8')
    return encoded_photo

def deactivate_led():
    # Desactivar el LED en el ESP32-CAM
    led_url = f"http://{esp32_cam_ip}:{esp32_cam_port}/control?var=led=off"
    try:
        requests.get(led_url, timeout=10)
        print("LED desactivado en el ESP32-CAM.")
    except requests.exceptions.RequestException as e:
        print(f"Error al desactivar el LED: {e}")

def main():    
    # Encontrar la ruta del lector de códigos de barras
    barcode_reader_path = find_barcode_reader_path()

    if barcode_reader_path:
        print(f"Lector de códigos de barras encontrado en: {barcode_reader_path}")

        # Crear un hilo para la lectura continua de códigos de barras
        barcode_thread = threading.Thread(target=read_barcode_thread, args=(barcode_reader_path,))
        barcode_thread.start()

        integer_value = 0  # Agregar esta línea para inicializar integer_value

        try:
            while True:
                # Esperar a que se lea el código de barras antes de ejecutar la captura de foto
                barcode_thread.join()
                resultado_integer_value = result_queue.get()

                # Imprimir el código de barras leído
                print(f"\nCódigo de barras leído: {resultado_integer_value}")

                # Agregar un tiempo de espera de 10 segundos antes de capturar la foto
                print("Esperando 10 segundos antes de capturar la foto...")
                time.sleep(10)

                # Ejecutar la captura de foto en cada iteración del bucle principal
                capture_photo(resultado_integer_value)
                deactivate_led()  # Desactivar el LED después de la captura

                # Reiniciar el hilo de lectura de códigos de barras para la próxima iteración
                barcode_thread = threading.Thread(target=read_barcode_thread, args=(barcode_reader_path,))
                barcode_thread.start()

        except KeyboardInterrupt:
            # Manejar la interrupción de teclado (Ctrl+C)
            print("\nPrograma principal interrumpido. Esperando a que el hilo de lectura de códigos de barras termine.")
            barcode_thread.join()

    else:
        print("Lector de códigos de barras no encontrado. Verifica la conexión del dispositivo.")

if __name__ == "__main__":
    main()
